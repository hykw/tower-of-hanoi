var Fade = enchant.Class.create(enchant.Sprite,
{
        initialize:function(x,y,game,FadeNumber)
    {
        enchant.Sprite.call(this,580,580);

        this.x = x;
        this.y = y;

        this.image = game.assets[imageFade];

        this._FadeMode = FadeNumber;

        switch(this._FadeMode)
        {
        //フェードイン
        case 0:
        //alert('フェードイン初期化');
        this.opacity = 0;
        break;
        //フェードアウト
        case 1:
        //alert('フェードアウト初期化');
        this.opacity = 1;
        break;
        }
    },
    onenterframe : function()
    {
      //フェードイン
      if(this._FadeMode == 0 && this.opacity <= 0.98)
        {
            this.opacity += 0.01;
        }
        else if(this._FadeMode == 0 && this.opacity >= 0.99)
        {
            this.scene.removeChild(this);
        }

      //フェードアウト
      if(this._FadeMode == 1 && this.opacity >= 0.02)
        {
            this.opacity -= 0.01;
        }
      else if(this._FadeMode == 1 && this.opacity <= 0.05)
        {
            this.scene.removeChild(this);
        }
    } 
});