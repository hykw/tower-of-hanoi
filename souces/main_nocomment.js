enchant();

window.onload = function()
{
    var width = 580;
    var height = 580;
    var game = new Core(width, height);
    game.fps = 60;
    //背景の色変更
    game.preload
    (
        //音
        MainBGM1,
        jingle1,
        jingle2,
        kettei,
        kin,
        ringo,
        kinspown,
        sinu,
        //テクスチャ
        imageTitleLogo,
        imageTitleBG,
        imageLotusLeaf,
        imageButton2,
        imageApple,
        imageClear,
        imageFrog,
        imageNeedle,
        imageBG,
        imageTree,
        imageCloud,
        imageScoreBoard,
        imageNumber,
        imageAsobikata1,
        imageAsobikata2,
        imageAsobikata3,
        imageAsobikata4,
        imageFade,
        imageNext,
        imageBack,
        imageFrame,
        imageHukidashi,
        imageHand,
        imageScore,
        imageLicense,
        imageApple_G,
        imageGetUI,
        imageAsobikataEX1,
        imageAsobikataEX2
    );

    game.onload = function()
    {
        //現在クリックしたかどうか
        var touch = false;
        //クリックした場所を記憶
        var nowTouchNum = -1;
        //土台の数
        field = [];
        //カエルの数
        frogs = [];
        //リンゴなどを管理する配列
        //items = [];
        //移動するカエルのID
        var moveFlogId = 0;
        //手数
        var handNum = 0;

        //おまけステージのカエルの数
        var extraFrogNum = 3;
        //簡単ステージのカエルの数
        var easyFrogNum = 3;
        //普通ステージのカエルの数
        var normalFrogNum = 4;
        //難しいステージのカエルの数
        var hardFrogNum = 5;
        //蓮の葉に乗るための調整用
        var offsetX = (580 / 3) / 11;
        var offsetY = (580 / 3) / 9;

        //bgm関連
        var bgm = game.assets[MainBGM1].clone();
        var result = game.assets[jingle1].clone();


        //se関連
        var getapple = game.assets[ringo].clone();
        var getgold = game.assets[kin].clone();
        var goldspown = game.assets[kinspown].clone();
        var point = game.assets[kettei].clone();
        var dead = game.assets[sinu].clone();

        //スコア
        var score = 0;
                
        //ツリー移動
        var Tree = new Sprite(580, 580);
        Tree.image = game.assets[imageTree];

        //初期化
        function Initialize(FlogNum, scene, exflag)
        {
            touch = false;
            nowTouchNum = -1;
            field = [];
            frogs = [];
            moveFlogId = 0;
            handNum = 0;

            var BG = new Sprite(580, 580);
            BG.image = game.assets[imageBG];
            scene.addChild(BG);

            scene.addChild(Tree);

            for(var i = 0; i < 3; i++)
            {
                var dodai = new Dodai(580 / 3 * i,height * (2/3) + 40, game);
                dodai._Id = i;
                field.push(dodai);
                scene.addChild(dodai);
            }

            //カエルの大きさを変更
            var frogScale = 140 * FlogNum / 2;
            //一時保存
            var frogsto = [];
            var goalfrog = [];
            for(var i = 0; i < FlogNum; i++)
            {
                if(frogsto.length > 0)
                {
                    var player = new Player(field[0].x + offsetX, frogsto[frogsto.length - 1].y - frogScale, ((FlogNum - i)/FlogNum), game);
                    if(exflag)
                    {
                        var goal = new Sprite(160, 140);
                        goal.image = game.assets[imageFrog];
                        goal.frame = 11;
                        goal.x = field[2].x + offsetX;
                        goal.y = goalfrog[goalfrog.length - 1].y - frogScale;
                        goal.scaleX = ((FlogNum - i)/FlogNum);
                        goal.scaleY = ((FlogNum - i)/FlogNum);
                    }
                }
                else
                {
                    var player = new Player(field[0].x + offsetX, field[0].y - offsetY + 10, ((FlogNum - i)/FlogNum), game);
                    if(exflag)
                    {
                        var goal = new Sprite(160, 140);
                        goal.image = game.assets[imageFrog];
                        goal.frame = 10;
                        goal.x = field[2].x + offsetX;
                        goal.y = field[2].y - offsetY + 10;
                        goal.scaleX = ((FlogNum - i)/FlogNum);
                        goal.scaleY = ((FlogNum - i)/FlogNum);
                    }
                }
                frogScale = 140 * ((FlogNum - i)/FlogNum) / 2.5;
                
                player._id = FlogNum - i;
                player._dodaiId = 0;
                frogsto.push(player);
                field[0]._put.push(FlogNum - i);

                if(exflag)
                {
                    goalfrog.push(goal);
                }
            }

            if(exflag)
            {
                for(var i = FlogNum - 1; i >= 0; i--)
                {
                    scene.addChild(goalfrog[i]);
                }
            }

            for(var i = FlogNum - 1; i >= 0; i--)
            {
                frogs.push(frogsto[i]);
                scene.addChild(frogsto[i]);
            }
        }

        function move(fieldnum, FrogNum)
        {
            //同じ場所をクリックしていたら
            if(nowTouchNum == fieldnum)
            {
                touch = false;
                frogs[moveFlogId - 1]._chooseFlag = false;
                return;
            }

            //なにも置かれていなかったら
            if(field[fieldnum]._put.length == 0)
            {
                frogs[moveFlogId - 1].x = field[fieldnum].x + offsetX;
                frogs[moveFlogId - 1].y = field[fieldnum].y + ((FrogNum - (frogs[moveFlogId - 1]._id - 1))/FrogNum) + ((FrogNum - 1) * 10) - (10 * frogs[moveFlogId - 1]._id);

                frogs[moveFlogId - 1]._chooseFlag = false;

                //前回置いてあったところからpopする
                field[frogs[moveFlogId - 1]._dodaiId]._put.pop();
                //id代入
                field[fieldnum]._put.push(frogs[moveFlogId - 1]._id);
                frogs[moveFlogId - 1]._dodaiId = field[fieldnum]._Id;

                if(handNum < 1000)
                {
                    handNum++;
                }
                touch = false;
            }
            //置かれていた場合
            else
            {
                //一番上に一番小さいのが乗っていたら
                if(field[fieldnum]._put[field[fieldnum]._put.length - 1] == 1)
                {
                    return;
                }
                else
                {
                    //移動先のカエルが移動するカエルより大きかったら
                    if(field[fieldnum]._put[field[fieldnum]._put.length - 1] > frogs[moveFlogId - 1]._id)
                    {
                        frogs[moveFlogId - 1].x = field[fieldnum].x + offsetX;
                        frogs[moveFlogId - 1].y = frogs[field[fieldnum]._put[field[fieldnum]._put.length - 1] - 1].y - 140 * ((FrogNum - (FrogNum - field[fieldnum]._put[field[fieldnum]._put.length - 1]))/FrogNum) / 2.5;
                        frogs[moveFlogId - 1]._chooseFlag = false;

                        //前回置いてあったところからpopする
                        field[frogs[moveFlogId - 1]._dodaiId]._put.pop();
                        //id代入
                        field[fieldnum]._put.push(frogs[moveFlogId - 1]._id);
                        frogs[moveFlogId - 1]._dodaiId = field[fieldnum]._Id;

                        if(handNum < 1000)
                        {
                            handNum++;
                        }   
                        touch = false;
                    }
                }
            }
        }

        //タワーの一番上を指定するとき通る
        function towerTopTouch(num)
        {
            if(field[num]._put.length == 0)
            {
                return;
            }
            else
            {
                //一番上にあるやつ
                frogs[field[num]._put[field[num]._put.length - 1] - 1]._chooseFlag = true;
                moveFlogId = field[num]._put[field[num]._put.length - 1];
                touch = true;
                nowTouchNum = num;
            }
        }

        //クリックした場所を管理
        function clickArea(scene, FrogNum)
        {
            scene.addEventListener('touchstart', function(e)
            {
                //右
                if(e.localX > width / 3 * 2)
                {
                    if(touch)
                    {
                        move(2, FrogNum);
                    }
                    else
                    {
                        towerTopTouch(2);
                    }
                }
                //左
                else if(e.localX < width / 3)
                {
                    
                    if(touch)
                    {
                        move(0, FrogNum);
                    }
                    else
                    {
                        towerTopTouch(0);
                    }
                }
                //中
                else
                {
                    if(touch)
                    {
                        move(1, FrogNum);
                    }
                    else
                    {
                        towerTopTouch(1);
                    }                  
                }
               
            });
        }

        //雲スポナ
       function CS(scene,count)
        {
            if(count == 0)
            {
                 var Sponer = new CloudSponer(500,0,game);
                 scene.addChild(Sponer);
                 scene.insertBefore(Sponer,Tree);
            }
        }

        //フェード
        function FadeInOut(scene,FadeNumber)
        {
            var fade = new Fade(0,0,game,FadeNumber);
            scene.addChild(fade);
        }

        //アイテムゲット時UI
        function GetUI(scene,x,y,Type)
        {
            var getUI = new ItemGet(x,y,game,Type);
            scene.addChild(getUI);
        }

        //おまけモード
        var extraMode = function()
        {
            var scene = new Scene();
 
            Initialize(extraFrogNum, scene, false);

            var board = new Sprite(580,580);
            board.image = game.assets[imageScoreBoard];
            scene.addChild(board);

            score = 0;

            var h_number = new Sprite(58, 88);
            h_number.image = game.assets[imageNumber];
            h_number.frame = 0;
            h_number.x = -15;
            h_number.y  = 23;
            h_number.scaleX = 0.4;
            h_number.scaleY = 0.4;
            scene.addChild(h_number);

            var t_number = new Sprite(58, 88);
            t_number.image = game.assets[imageNumber];
            t_number.frame = 0;
            t_number.x = 13;
            t_number.y  = 23;
            t_number.scaleX = 0.4;
            t_number.scaleY = 0.4;
            scene.addChild(t_number);

            var o_number = new Sprite(58, 88);
            o_number.image = game.assets[imageNumber];
            o_number.frame = 0;
            o_number.x = 40;
            o_number.y  = 23;
            o_number.scaleX = 0.4;
            o_number.scaleY = 0.4;
            scene.addChild(o_number);

            var title = new Sprite(120,60);
            title.image = game.assets[imageButton2];
            title.x = width - 130;
            title.y = 50;
            title.scaleX = 0.8;
            title.scaleY = 0.8;
            title.frame = 14;
            scene.addChild(title);


            //フェード制御用
            var FadeControl = 0;
            //フェードアウト
            if(FadeControl == 0)
            {
                FadeInOut(scene,1);
                FadeControl = 1;
            }

            items = [];

            var flg = false;
            var Item;
            var count = 0;
            //雲用の変数----------------------------------------
            var CloudCount = 0;
            var CloudTimeCount = 0;
            //-------------------------------------------------

            //スポーンカウンター変数
            var SpawnCounter = 0;//初期値1000

            //経過時間用の変数
            var progress = 0;

            scene.addEventListener('enterframe', function()
            {
                //経過した時間を取得する
                progress ++;
                //1秒で60加算
                //10秒で600
                //30秒で1800

                //経過時間によってスポーンカウンター変数を逐次変更
                if(progress >= 0 && progress < 1800)
                {
                    SpawnCounter = 1000;
                }
                else if(progress >= 1800 && progress < 3600)
                {
                    SpawnCounter = 900;
                }
                else if(progress >= 3600 && progress < 5400)
                {
                    SpawnCounter = 800;
                }
                else if(progress >= 5400 && progress < 7200)
                {
                    SpawnCounter = 700;
                }
                else if(progress >= 7200 && progress < 9000)
                {
                    SpawnCounter = 600;
                }
                else if(progress >= 9000 && progress < 10800)
                {
                    SpawnCounter = 500;
                }
                else if(progress >= 10800 && progress < 12600)
                {
                    SpawnCounter = 400;
                }
                else if(progress >= 12600 && progress < 14400)
                {
                    SpawnCounter = 300;
                }
                else if(progress >= 14400 && progress < 16200)
                {
                    SpawnCounter = 200;
                }
                else if(progress >= 16200 && progress < 18000)
                {
                    SpawnCounter = 100;
                }

                //暫定的雲スポーン-------------------------------
                if(CloudTimeCount == 0)
                {
                    CloudCount = 0;
                    CS(scene,CloudCount);
                    CloudCount = 1;
                }
                CloudTimeCount++;
                if(CloudTimeCount >= 1250)//ここの値が雲の沸き時間
                {
                    CloudTimeCount = 0;
                }

                //フラグがfalseの時スポーンの処理を行う
                if(!flg)
                {
                    numCheck();
                }
                //フラグがtrueの時再度スポーンまでのカウントを行う
                if(flg)
                {
                    //countが再度スポーンまでの時間
                    count ++;
                    if(count >= SpawnCounter)
                    {
                        flg = false;
                        count = 0;
                    }
                }

                var handred = parseInt(score / 100);
                var ten = parseInt((score - (handred * 100)) / 10);
                var one = parseInt(score - ((handred * 100) + (ten  * 10)));

                h_number.frame = handred;
                t_number.frame = ten;
                o_number.frame = one;

                if(score >= 999)
                {
                    score = 999;
                }

                window.document.onmousemove = function(e)
                {
                    if(
                        e.offsetX > title.x && 
                        e.offsetX < title.x + 100 && 
                        e.offsetY > title.y && 
                        e.offsetY < title.y + 60)
                    {
                            title.frame = 10;
                    }
                    else
                    {
                        title.frame = 14;
                    }       
                }
            });

            title.addEventListener('touchstart', function()
            {
                score = 0;
                game.replaceScene(TitleStart());
            });

            function numCheck()
            {
                var Pos = posOutput(7);
                if(Pos <= 3)
                {
                    fieldCheck_A(imageApple, false, 1, 1);
                    flg = true;
                }
                else if(Pos == 6)
                {
                    fieldCheck_A(imageApple_G, false, 3, 1.75);
                    flg = true;
                    goldspown.play();
                    goldspown.volume = 0.5;
                }
                else
                {
                    fieldCheck_N(imageNeedle, true, 0, 1);
                    flg = true;
                }
            }

            //リンゴ用
            function fieldCheck_A(Image, flag, score, speed)
            {
                //カエルのいないところを探す
                var check = [];
                for(var i = 0; i < 3; i++)
                {
                    if(field[i]._put.length == 0)
                    {
                        check.push(i);
                    }
                }

                if(check.length == 0)
                {
                    spawn(posOutput(3), Image, flag, score, speed);
                }
                else
                {
                    spawn(check[posOutput(check.length + 1) - 1] + 1, Image, flag, score, speed);
                }
            }

            //トゲ用
            function fieldCheck_N(Image, flag, score, speed)
            {
                //カエルのいないところを探す
                var check = [];
                for(var i = 0; i < 3; i++)
                {
                    if(field[i]._put.length > 0)
                    {
                        check.push(i);
                    }
                }

                spawn(check[posOutput(check.length + 1) - 1] + 1, Image, flag, score, speed);
            }

            function posOutput(max)
            {
                //ポジションを乱数で決める
                var Posran = Math.random() * (max - 1) + 1; 
                var Pos = Math.floor(Posran);

                return Pos;
            }

            function spawn(Pos, Image, flag, score, speed)
            {
                //その乱数でポジションを設定
                switch(Pos)
                {
                    //リンゴ
                    case 1:
                    Item = new ScoreItem(38,-180,game);
                    Item.image = game.assets[Image];
                    Item._dodaiId  = Pos - 1;
                    Item._score = score;
                    Item._itemFlag = flag;
                    Item._Speed *= speed;
                    scene.addChild(Item);
                    scene.insertBefore(Item, frogs[0]);
                    items.push(Item);
                    break;

                    case 2:
                    Item = new ScoreItem(228,-180,game);
                    Item.image = game.assets[Image];
                    Item._dodaiId  = Pos - 1;
                    Item._score = score;
                    Item._itemFlag = flag;
                    Item._Speed *= speed;
                    scene.addChild(Item);
                    scene.insertBefore(Item,frogs[0]);
                    items.push(Item);
                    break;

                    case 3:
                    Item = new ScoreItem(432,-180,game);
                    Item.image = game.assets[Image];
                    Item._dodaiId  = Pos - 1;
                    Item._score = score;
                    Item._itemFlag = flag;
                    Item._Speed *= speed;
                    scene.addChild(Item);
                    scene.insertBefore(Item,frogs[0]);
                    items.push(Item);
                    break;
                }
            }

            frogs[0].addEventListener('enterframe', function()
            {
                //当たり判定
                for(var i = 0; i < items.length; i++)
                {      
                    if(
                        frogs[0].y + 140 * (1/3) <= items[i].y + items[i].height &&
                        frogs[0].y + (140 * (1/3)) * 2 >= items[i].y &&
                        items[i]._hitFlag &&
                        frogs[0]._dodaiId == items[i]._dodaiId)
                    {
                        if(items[i]._itemFlag)
                        {
                            game.replaceScene(extraResult());
                        }
                        else if(field[frogs[0]._dodaiId]._put.length == 3 && 
                            !items[i]._itemFlag)
                        {
                            score += items[i]._score;
                            if(items[i]._score == 1)
                            {
                                //ゲットUI生成
                                GetUI(scene,frogs[0].x - 70,frogs[0].y - 20,0);
                                
                                getapple.play();
                            }
                            else
                            {
                                GetUI(scene,frogs[0].x - 70,frogs[0].y - 20,2);

                                getgold.play();
                            }
                            scene.removeChild(items[i]);
                            items.splice(i, 1);         
                        }
                    }
                }
            });

            frogs[1].addEventListener('enterframe', function()
            {
                 //当たり判定
                 for(var i = 0; i < items.length; i++)
                 {      
                    if(
                        frogs[1].y + 45 <= items[i].y + items[i].height && 
                        frogs[1].y + 45 * 2 >= items[i].y &&
                        items[i]._hitFlag &&
                        frogs[1]._dodaiId == items[i]._dodaiId && 
                        items[i]._itemFlag)
                     {
                        game.replaceScene(extraResult());
                     }
                 }
            });
            
            frogs[2].addEventListener('enterframe', function()
            {
                 //当たり判定
                 for(var i = 0; i < items.length; i++)
                 {      
                    if(
                        frogs[2].y + 30 <= items[i].y + items[i].height && 
                        frogs[2].y + 30 * 2 >= items[i].y &&
                        items[i]._hitFlag &&
                        frogs[2]._dodaiId == items[i]._dodaiId && 
                       items[i]._itemFlag)
                     {
                        game.replaceScene(extraResult());
                     }
                 }
            });


            clickArea(scene, extraFrogNum);
            return scene;
        }

        //簡単モード
        var easyMode = function()
        {
            var scene = new Scene();
            
            Initialize(easyFrogNum, scene, true);

            var modoru = new Sprite(120,60);
            modoru.image = game.assets[imageButton2];
            modoru.x = 30;
            modoru.y = 50;
            modoru.scaleX = 0.8;
            modoru.scaleY = 0.8;
            modoru.frame = 20;
            scene.addChild(modoru);

            var title = new Sprite(120,60);
            title.image = game.assets[imageButton2];
            title.x = width - 150;
            title.y = 50;
            title.scaleX = 0.8;
            title.scaleY = 0.8;
            title.frame = 14;
            scene.addChild(title);
            
             //雲用の変数----------------------------------------
             var CloudCount = 0;
             var CloudTimeCount = 0;
             //-------------------------------------------------

            //フェード制御用
            var FadeControl = 0;
            //フェードアウト
            if(FadeControl == 0)
            {
                FadeInOut(scene,1);
                FadeControl = 1;
            }
            scene.addEventListener('enterframe', function()
            {
                //暫定的雲スポーン-------------------------------
                if(CloudTimeCount == 0)
                {
                    CloudCount = 0;
                    CS(scene,CloudCount);
                    CloudCount = 1;
                }
                CloudTimeCount++;
                if(CloudTimeCount >= 1250)//ここの値が雲の沸き時間
                {
                    CloudTimeCount = 0;
                }
                
                if(field[2]._put.length == easyFrogNum)
                {
                    //クリア画面表示
                    game.replaceScene(ClearScene());
                }

                window.document.onmousemove = function(e)
                {
                    if(
                        e.offsetX > modoru.x && 
                        e.offsetX < modoru.x + 100 && 
                        e.offsetY > modoru.y && 
                        e.offsetY < modoru.y + 60)
                        {
                            modoru.frame = 16;
                        }
                        else
                        {
                            modoru.frame = 20;
                        }

                    if(
                        e.offsetX > title.x && 
                        e.offsetX < title.x + 100 && 
                        e.offsetY > title.y && 
                        e.offsetY < title.y + 60)
                        {
                            title.frame = 10;
                        }
                        else
                        {
                            title.frame = 14;
                        }
                }
            });

            modoru.addEventListener('touchstart', function()
            {
                game.replaceScene(easyMode());
            });

            title.addEventListener('touchstart', function()
            {
                game.replaceScene(TitleStart());
            });
                
            clickArea(scene, easyFrogNum);
            return scene;
        }

        //普通モード
        var normalMode = function()
        {
            var scene = new Scene();
            
            Initialize(normalFrogNum, scene, true);

            var modoru = new Sprite(120,60);
            modoru.image = game.assets[imageButton2];
            modoru.x = 30;
            modoru.y = 50;
            modoru.scaleX = 0.8;
            modoru.scaleY = 0.8;
            modoru.frame = 20;
            scene.addChild(modoru);

            var title = new Sprite(120,60);
            title.image = game.assets[imageButton2];
            title.x = width - 150;
            title.y = 50;
            title.scaleX = 0.8;
            title.scaleY = 0.8;
            title.frame = 14;
            scene.addChild(title);

             //雲用の変数----------------------------------------
             var CloudCount = 0;
             var CloudTimeCount = 0;
             //-------------------------------------------------

            //フェード制御用
            var FadeControl = 0;
            //フェードアウト
            if(FadeControl == 0)
            {
                FadeInOut(scene,1);
                FadeControl = 1;
            }

            scene.addEventListener('enterframe', function()
            {
                //暫定的雲スポーン-------------------------------
                if(CloudTimeCount == 0)
                {
                    CloudCount = 0;
                    CS(scene,CloudCount);
                    CloudCount = 1;
                }
                CloudTimeCount++;
                if(CloudTimeCount >= 1250)//ここの値が雲の沸き時間
                {
                    CloudTimeCount = 0;
                }
                if(field[2]._put.length == normalFrogNum)
                {
                    game.replaceScene(ClearScene());
                }

                window.document.onmousemove = function(e)
                {
                    if(
                        e.offsetX > modoru.x && 
                        e.offsetX < modoru.x + 100 && 
                        e.offsetY > modoru.y && 
                        e.offsetY < modoru.y + 60)
                        {
                            modoru.frame = 16;
                        }
                        else
                        {
                            modoru.frame = 20;
                        }

                    if(
                        e.offsetX > title.x && 
                        e.offsetX < title.x + 100 && 
                        e.offsetY > title.y && 
                        e.offsetY < title.y + 60)
                        {
                            title.frame = 10;
                        }
                        else
                        {
                            title.frame = 14;
                        }
                }
            });

            modoru.addEventListener('touchstart', function()
            {
                game.replaceScene(normalMode());
            });

            title.addEventListener('touchstart', function()
            {
                game.replaceScene(TitleStart());
            });
                
            clickArea(scene, normalFrogNum);
            return scene;
        }

        //難しいモード
        var hardMode = function()
        {
            var scene = new Scene();
            
            Initialize(hardFrogNum, scene, true);

            var modoru = new Sprite(120,60);
            modoru.image = game.assets[imageButton2];
            modoru.x = 30;
            modoru.y = 50;
            modoru.scaleX = 0.8;
            modoru.scaleY = 0.8;
            modoru.frame = 20;
            scene.addChild(modoru);

            var title = new Sprite(120,60);
            title.image = game.assets[imageButton2];
            title.x = width - 150;
            title.y = 50;
            title.scaleX = 0.8;
            title.scaleY = 0.8;
            title.frame = 14;
            scene.addChild(title);

             //雲用の変数----------------------------------------
             var CloudCount = 0;
             var CloudTimeCount = 0;
             //-------------------------------------------------

             //フェード制御用
             var FadeControl = 0;
             //フェードアウト
             if(FadeControl == 0)
             {
                 FadeInOut(scene,1);
                 FadeControl = 1;
             }

            scene.addEventListener('enterframe', function()
            {
                //暫定的雲スポーン-------------------------------
                if(CloudTimeCount == 0)
                {
                    CloudCount = 0;
                    CS(scene,CloudCount);
                    CloudCount = 1;
                }
                CloudTimeCount++;
                if(CloudTimeCount >= 1250)//ここの値が雲の沸き時間
                {
                    CloudTimeCount = 0;
                }
                if(field[2]._put.length == hardFrogNum)
                {
                    game.replaceScene(ClearScene());
                }

                window.document.onmousemove = function(e)
                {
                    if(
                        e.offsetX > modoru.x && 
                        e.offsetX < modoru.x + 100 && 
                        e.offsetY > modoru.y && 
                        e.offsetY < modoru.y + 60)
                        {
                            modoru.frame = 16;
                        }
                        else
                        {
                            modoru.frame = 20;
                        }

                        if(
                            e.offsetX > title.x && 
                            e.offsetX < title.x + 100 && 
                            e.offsetY > title.y && 
                            e.offsetY < title.y + 60)
                            {
                                title.frame = 10;
                            }
                            else
                            {
                                title.frame = 14;
                            }
                }
            });

            modoru.addEventListener('touchstart', function()
            {
                game.replaceScene(hardMode());
            });

            title.addEventListener('touchstart', function()
            {
                game.replaceScene(TitleStart());
            });
                
            clickArea(scene, hardFrogNum);
            return scene;
        }


        //タイトル
        var TitleScene = function()
        {
            var scene = new Scene();

            //フェード用変数
            var FadeControl2 = 0;
            var FadeTimer2 = 0;
            var FadeFlg = false;
            var FadeTitleOnes = 0;

            var BG = new Sprite(580, 580);
            BG.image = game.assets[imageBG];
            scene.addChild(BG);

            scene.addChild(Tree);
  
            var title = new Sprite(580,580);
            title.image = game.assets[imageTitleLogo];
            title.x = 0;
            title.y = 0;
            scene.addChild(title);
            
            var buttonWidth = 120;
            var buttonHeight = 60;
            var button = [];
            var easy = new TitleButton(50, height * (2/3) - 20, buttonWidth, buttonHeight, game, imageButton2);
            easy.frame = 4;
            button.push(easy);
            scene.addChild(easy);
            var normal = new TitleButton(width / 2 - 60, height * (2/3) - 20, buttonWidth, buttonHeight, game, imageButton2);
            normal.frame = 5;
            button.push(normal);
            scene.addChild(normal);
            var hard = new TitleButton(width * (2/3) + 20, height * (2/3) - 20, buttonWidth, buttonHeight, game, imageButton2);
            hard.frame = 6;
            button.push(hard);
            scene.addChild(hard);
            var extra = new TitleButton(width / 2 - 60,height * (5/6) - 5, buttonWidth, buttonHeight, game, imageButton2);
            extra.frame = 7;
            button.push(extra);
            scene.addChild(extra);
            var modoru = new TitleButton(width - 150, height * (5/6) - 5, buttonWidth, buttonHeight, game, imageButton2);
            modoru.frame = 14;
            modoru.scaleX = 0.8;
            modoru.scaleY = 0.8;
            button.push(modoru);
            scene.addChild(modoru);

            
            var license = new Sprite(580, 30);
            license.image = game.assets[imageLicense];
            license.x  = 120;
            license.y = height - 25;
            license.scaleX = 0.6;
            license.scaleY = 0.6;
            license.opacity = 0.5;
            
            scene.addChild(license);
            
            var CloudCount = 0;
            var CloudTimeCount = 0;

            function mouseover(button, e)
            {
                if(e.offsetX > button.x && e.offsetX < button.x + 120 && e.offsetY > button.y && e.offsetY < button.y + 60)
                {
                    changeButton(button, 0, 1, 2, 3, 10);
                }
                else
                {
                    changeButton(button, 4, 5, 6, 7, 14);
                }
            }

            function changeButton(button, e_frame, n_frame, h_frame, o_frame, m_frame)
            {
                switch(button)
                {
                    case easy:
                    easy.frame = e_frame;
                    break;

                    case normal:
                    normal.frame = n_frame;
                    break;

                    case hard:
                    hard.frame = h_frame;
                    break;

                    case extra:
                    extra.frame = o_frame;
                    break;

                    case modoru:
                    modoru.frame = m_frame;
                    break;
                }
            }

            function changeScene()
            {
                for(var i = 0; i < button.length; i++)
                {
                    if(button[i]._sceneFlag)
                    {
                        switch(i)
                        {
                            case 0:
                            FadeFlg = true;
                            if(FadeControl2 == 0 && FadeTimer2 <= 100)
                            {
                                point.play();
                                FadeInOut(scene,0);
                                FadeControl2 = 1;
                            }
                            if(FadeControl2 == 1 && FadeTimer2 >= 100)
                            {
                                game.replaceScene(easyMode());
                            }
                            break;

                            case 1:
                            FadeFlg = true;
                            if(FadeControl2 == 0 && FadeTimer2 <= 100)
                            {
                                point.play();
                                FadeInOut(scene,0);
                                FadeControl2 = 1;
                            }
                            if(FadeControl2 == 1 && FadeTimer2 >= 100)
                            game.replaceScene(normalMode());
                            break;

                            case 2: 
                            FadeFlg = true;
                            if(FadeControl2 == 0 && FadeTimer2 <= 100)
                            {
                                point.play();
                                FadeInOut(scene,0);
                                FadeControl2 = 1;
                            }
                            if(FadeControl2 == 1 && FadeTimer2 >= 100)
                            game.replaceScene(hardMode());
                            break;

                            case 3:
                            FadeFlg = true;
                            if(FadeControl2 == 0 && FadeTimer2 <= 100)
                            {
                                point.play();
                                FadeInOut(scene,0);
                                FadeControl2 = 1;
                            }
                            if(FadeControl2 == 1 && FadeTimer2 >= 100)
                            game.replaceScene(extraMode());
                            break;

                            case 4:
                            point.play();
                            game.replaceScene(TitleStart());
                            break;
                        }
                    }
                }
            }

            scene.addEventListener('enterframe', function()
            {
                window.document.onmousemove = function(e)
                {
                    mouseover(easy, e);
                    mouseover(normal, e);
                    mouseover(hard, e);
                    mouseover(extra, e);
                    mouseover(modoru, e);
                }
                if(FadeFlg)
                {
                    FadeTimer2 ++;
                }

                changeScene();

                if(CloudTimeCount == 0)
                {
                    CloudCount = 0;
                    CS(scene,CloudCount);
                    CloudCount = 1;
                }
                CloudTimeCount++;
                if(CloudTimeCount >= 1250)//ここの値が雲の沸き時間
                {
                    CloudTimeCount = 0;
                }
            });

            return scene;
        }

        //タイトルスタートある方
        var TitleStart = function()
        {
            var scene = new Scene();

            //フェード用変数
            var FadeControl2 = 0;
            var FadeTimer2 = 0;
            var FadeFlg = false;
            var FadeTitleOnes = 0;

            bgm.play();

            var BG = new Sprite(580, 580);
            BG.image = game.assets[imageBG];
            scene.addChild(BG);

            scene.addChild(Tree);
  
            var title = new Sprite(580,580);
            title.image = game.assets[imageTitleLogo];
            title.x = 0;
            title.y = 0;
            scene.addChild(title);
            
            var buttonWidth = 120;
            var buttonHeight = 60;
            var button = [];
            var asobikata = new TitleButton(100,height * (2/3), buttonWidth, buttonHeight, game, imageButton2);
            asobikata.frame = 13;
            asobikata.scaleX = 1.5;
            asobikata.scaleY = 1.5;
            button.push(asobikata);
            scene.addChild(asobikata);
            var start = new TitleButton(width / 2 + 70,height * (2/3), buttonWidth, buttonHeight, game, imageButton2);
            start.frame = 15;
            start.scaleX = 1.5;
            start.scaleY = 1.5;
            button.push(start);
            scene.addChild(start);
            
            var license = new Sprite(580, 30);
            license.image = game.assets[imageLicense];
            license.x  = 120;
            license.y = height - 25;
            license.scaleX = 0.6;
            license.scaleY = 0.6;
            license.opacity = 0.5;
            
            scene.addChild(license);
            
            var CloudCount = 0;
            var CloudTimeCount = 0;

            //最初のフェードアウト
            if(FadeTitleOnes == 0)
            {
                FadeInOut(scene,1);
                FadeTitleOnes = 1;
            }

            function mouseover(button, e)
            {
                if(e.offsetX > button.x && e.offsetX < button.x + 120 && e.offsetY > button.y && e.offsetY < button.y + 60)
                {
                    changeButton(button, 9, 11);
                }
                else
                {
                    changeButton(button, 13, 15);
                }
            }

            function changeButton(button, a_frame, s_frame)
            {
                switch(button)
                {
                    case asobikata:
                    asobikata.frame = a_frame;
                    break;

                    case start:
                    start.frame = s_frame;
                    break;
                }
            }

            function changeScene()
            {
                for(var i = 0; i < button.length; i++)
                {
                    if(button[i]._sceneFlag)
                    {
                        switch(i)
                        {
                            case 0:
                            point.play();
                            game.replaceScene(asobikataMode());
                            break;

                            case 1:
                            point.play()
                            game.replaceScene(TitleScene());
                            break;
                        }
                    }
                }
            }

            scene.addEventListener('enterframe', function()
            {
                window.document.onmousemove = function(e)
                {
                    mouseover(asobikata, e);
                    mouseover(start, e);
                }
                if(FadeFlg)
                {
                    FadeTimer2 ++;
                }

                changeScene();

                if(CloudTimeCount == 0)
                {
                    CloudCount = 0;
                    CS(scene,CloudCount);
                    CloudCount = 1;
                }
                CloudTimeCount++;
                if(CloudTimeCount >= 1250)//ここの値が雲の沸き時間
                {
                    CloudTimeCount = 0;
                }
            });

            return scene;
        } 

        var ClearScene = function()
        {
            var scene = new Scene();

            bgm.stop();
            result.play();

            //フェード用変数
            var FadeControl3 = 0;
            var FadeTimer = 0;
            var flg = false;

            var BG = new Sprite(580,580);
            BG.image = game.assets[imageBG];
            scene.addChild(BG);

            scene.addChild(Tree);

            var clear = new Sprite(580, 580);
            clear.image = game.assets[imageClear];
            clear.y = -height / 2.5;
            scene.addChild(clear);

            var fukidasi = new Sprite(580, 580);
            fukidasi.image = game.assets[imageHukidashi];
            scene.addChild(fukidasi);
            
            var hand = new Sprite(580, 580);
            hand.image = game.assets[imageHand];
            scene.addChild(hand);

            var handred = parseInt(handNum / 100);
            var ten = parseInt((handNum - (handred * 100)) / 10);
            var one = parseInt(handNum - ((handred * 100) + (ten  * 10)));
            var h_number = new Sprite(58, 88);
            h_number.image = game.assets[imageNumber];
            h_number.frame = handred;
            h_number.x = width / 2 - 58 * 2 + 5;
            h_number.y  = height / 2 - 44;
            h_number.scaleX = 1.3;
            h_number.scaleY = 1.3;
            scene.addChild(h_number);

            var t_number = new Sprite(58, 88);
            t_number.image = game.assets[imageNumber];
            t_number.frame = ten;
            t_number.x = width / 2 - 29;
            t_number.y  = height / 2 - 44;
            t_number.scaleX = 1.3;
            t_number.scaleY = 1.3;
            scene.addChild(t_number);

            var o_number = new Sprite(58, 88);
            o_number.image = game.assets[imageNumber];
            o_number.frame = one;
            o_number.x = width / 2 + 58 - 5;
            o_number.y  = height / 2 - 44;
            o_number.scaleX = 1.3;
            o_number.scaleY = 1.3;
            scene.addChild(o_number);

            for(var i = 0; i < 3; i++)
            {
                var dodai = new Dodai(580 / 3 * i,height * (2/3) + 40, game);
                dodai.Id = i;
                scene.addChild(dodai);
            }

            var frog = new Sprite(160, 140);
            frog.image = game.assets[imageFrog];
            frog.frame = 4;
            frog.scaleX = 0.8;
            frog.scaleY = 0.8;
            frog.x = field[2].x + (580 / 3) / 13;
            frog.y = field[2].y;
            scene.addChild(frog);

            var modoru = new Sprite(120,60);
            modoru.image = game.assets[imageButton2];
            modoru.x = field[0].x + (580 / 3) / 5;
            modoru.y = field[0].y + (580 / 3) / 5;
            modoru.frame = 14;
            scene.addChild(modoru);

            var flag = false;
            var time = 0;

            var CloudCount = 0;
            var CloudTimeCount = 0;

            clear.addEventListener('enterframe', function()
            {
                this.tl.scaleTo(2, 20, enchant.Easing.LINEAR).scaleTo(1, 100);
            });

            modoru.addEventListener('touchstart',function()
            {
                if(FadeControl3 == 0)
                {
                    game.assets[kettei].play();
                    FadeInOut(scene,0);
                    FadeControl3 = 1;
                    flg = true;
                }
                
            });

            frog.addEventListener('enterframe', function()
            {
                time = parseInt(game.frame / game.fps);
                if(flag)
                {
                    frog.frame = 4;
        
                    if(time % 2 != 0)
                    {
                        flag = false;
                    }
                }
                else
                {
                    frog.frame = 3;
        
                    if(time % 2 == 0)
                    {
                        flag = true;
                    }
                }
            });

            scene.addEventListener('enterframe', function(){

                if(flg)
                {
                    FadeTimer++;
                }

                if(FadeControl3 == 1 && FadeTimer >= 120)
                {
                    game.replaceScene(TitleStart());
                }

                window.document.onmousemove = function(e)
                {
                    if(
                        e.offsetX > modoru.x && 
                        e.offsetX < modoru.x + 100 && 
                        e.offsetY > modoru.y && 
                        e.offsetY < modoru.y + 50)
                        {
                            modoru.frame = 10;
                        }
                        else
                        {
                            modoru.frame = 14;
                        }

                }

                if(CloudTimeCount == 0)
                {
                    CloudCount = 0;
                    CS(scene,CloudCount);
                    CloudCount = 1;
                }
                CloudTimeCount++;
                if(CloudTimeCount >= 1250)//ここの値が雲の沸き時間
                {
                    CloudTimeCount = 0;
                }
            });

            return scene;
        }

        var asobikataMode = function ()
        {
            var scene = new Scene();

            var boards = [];

            var boradframe = new Sprite(580, 580);
            boradframe.image = game.assets[imageFrame];
            scene.addChild(boradframe);

            var number = 1;

            var board = new Sprite(580, 580);
            board.image = game.assets[imageAsobikata1];
            board.scaleX = 0.7;
            board.scaleY = 0.7;
            board.x = -3;
            board.y = -3;
            boards.push(board);
            scene.addChild(board);


            var right = new Sprite(580, 580);
            right.image = game.assets[imageNext];
            right.x = width / 2 - 54.5;
            scene.addChild(right);

            var left = new Sprite(580, 580);
            left.image = game.assets[imageBack];
            left.x = -width / 2 + 55;
            scene.addChild(left);

            var modoru = new Sprite(120,60);
            modoru.image = game.assets[imageButton2];
            modoru.x = 50;
            modoru.y = 510;
            modoru.frame = 14;
            scene.addChild(modoru);

            scene.addEventListener('enterframe', function(){
                window.document.onmousemove = function(e)
                {
                    if(
                        e.offsetX > modoru.x && 
                        e.offsetX < modoru.x + 100 && 
                        e.offsetY > modoru.y && 
                        e.offsetY < modoru.y + 60)
                        {
                            modoru.frame = 10;
                        }
                        else
                        {
                            modoru.frame = 14;
                        }
                }
            });

            right.addEventListener('touchstart', function()
            {
                number++;
                chnage();
            });

            right.addEventListener('enterframe', function(){
                this.tl.moveBy(20,0,10,enchant.Easing.QUINT_EASEOUT).delay(30).moveBy(-20, 0, 10, enchant.Easing.BACK_EASEOUT).delay(30);
            });

            left.addEventListener('touchstart', function()
            {
                number--;
                chnage();
            });

            left.addEventListener('enterframe', function(){
                this.tl.moveBy(-20,0,10,enchant.Easing.QUINT_EASEOUT).delay(30).moveBy(20, 0, 10, enchant.Easing.BACK_EASEOUT).delay(30);
            });

            modoru.addEventListener('touchstart',function()
            {
                game.assets[kettei].play();
                game.replaceScene(TitleStart());
            });

            function chnage()
            {
                switch(number)
                {
                    case 1:
                    board.image = game.assets[imageAsobikata1];
                    break;

                    case 2:
                    board.image = game.assets[imageAsobikata2];
                    break;

                    case 3:
                    board.image = game.assets[imageAsobikata3];
                    break;

                    case 4:
                    board.image = game.assets[imageAsobikata4];
                    break;

                    case 5:
                    board.image = game.assets[imageAsobikataEX1];
                    break;

                    case 6:
                    board.image = game.assets[imageAsobikataEX2];
                    break;

                    default:
                    if(number < 1)
                    {
                        number = 6;
                    }
                    else if(number > 6)
                    {
                        number = 1;
                    }
                    chnage();
                    break;
                }
            }

            return scene;
        }

        var extraResult = function()
        {
            var scene = new Scene();

            bgm.stop();
            result.play();

            //フェード用変数
            var FadeControl3 = 0;
            var FadeTimer = 0;
            var flg = false;

            var BG = new Sprite(580,580);
            BG.image = game.assets[imageBG];
            scene.addChild(BG);

            scene.addChild(Tree);


            var fukidasi = new Sprite(580, 580);
            fukidasi.image = game.assets[imageHukidashi];
            scene.addChild(fukidasi);
            
            var hand = new Sprite(580, 580);
            hand.image = game.assets[imageScore];
            hand.scaleX = 0.5;
            hand.scaleY = 0.5;
            scene.addChild(hand);

            var handred = parseInt(score / 100);
            var ten = parseInt((score - (handred * 100)) / 10);
            var one = parseInt(score - ((handred * 100) + (ten  * 10)));
            var h_number = new Sprite(58, 88);
            h_number.image = game.assets[imageNumber];
            h_number.frame = handred;
            h_number.x = width / 2 - 58 * 2 + 5;
            h_number.y  = height / 2 - 44;
            h_number.scaleX = 1.3;
            h_number.scaleY = 1.3;
            scene.addChild(h_number);

            var t_number = new Sprite(58, 88);
            t_number.image = game.assets[imageNumber];
            t_number.frame = ten;
            t_number.x = width / 2 - 29;
            t_number.y  = height / 2 - 44;
            t_number.scaleX = 1.3;
            t_number.scaleY = 1.3;
            scene.addChild(t_number);

            var o_number = new Sprite(58, 88);
            o_number.image = game.assets[imageNumber];
            o_number.frame = one;
            o_number.x = width / 2 + 58 - 5;
            o_number.y  = height / 2 - 44;
            o_number.scaleX = 1.3;
            o_number.scaleY = 1.3;
            scene.addChild(o_number);

            for(var i = 0; i < 3; i++)
            {
                var dodai = new Dodai(580 / 3 * i,height * (2/3) + 40, game);
                dodai.Id = i;
                scene.addChild(dodai);
            }

            var frog = new Sprite(160, 140);
            frog.image = game.assets[imageFrog];
            frog.frame = 4;
            frog.scaleX = 0.8;
            frog.scaleY = 0.8;
            frog.x = field[2].x + (580 / 3) / 13;
            frog.y = field[2].y;
            scene.addChild(frog);

            var modoru = new Sprite(120,60);
            modoru.image = game.assets[imageButton2];
            modoru.x = field[0].x + (580 / 3) / 5;
            modoru.y = field[0].y + (580 / 3) / 5;
            modoru.frame = 14;
            scene.addChild(modoru);

            var flag = false;
            var time = 0;

            var CloudCount = 0;
            var CloudTimeCount = 0;

            modoru.addEventListener('touchstart',function()
            {
                if(FadeControl3 == 0)
                {
                    FadeInOut(scene,0);
                    FadeControl3 = 1;
                    flg = true;
                }
                
            });

            frog.addEventListener('enterframe', function()
            {
                time = parseInt(game.frame / game.fps);
                if(flag)
                {
                    frog.frame = 4;
        
                    if(time % 2 != 0)
                    {
                        flag = false;
                    }
                }
                else
                {
                    frog.frame = 3;
        
                    if(time % 2 == 0)
                    {
                        flag = true;
                    }
                }
            });

            scene.addEventListener('enterframe', function(){

                if(flg)
                {
                    FadeTimer++;
                }

                if(FadeControl3 == 1 && FadeTimer >= 120)
                {
                    game.replaceScene(TitleStart());
                }

                window.document.onmousemove = function(e)
                {
                    if(
                        e.offsetX > modoru.x && 
                        e.offsetX < modoru.x + 100 && 
                        e.offsetY > modoru.y && 
                        e.offsetY < modoru.y + 50)
                        {
                            modoru.frame = 10;
                        }
                        else
                        {
                            modoru.frame = 14;
                        }

                }

                if(CloudTimeCount == 0)
                {
                    CloudCount = 0;
                    CS(scene,CloudCount);
                    CloudCount = 1;
                }
                CloudTimeCount++;
                if(CloudTimeCount >= 1250)//ここの値が雲の沸き時間
                {
                    CloudTimeCount = 0;
                }
            });

            return scene;
        }
        game.replaceScene(TitleStart());
    };
    game.start();
};