var ItemGet = enchant.Class.create(enchant.Sprite,
    {
            initialize:function(x,y,game,Type)
        {
            enchant.Sprite.call(this,300,120);
    
            this.x = x;
            this.y = y;
    
            this.image = game.assets[imageGetUI];

            this.frame = Type;

            //大きさ変更
            this.scaleX = 0.25;
            this.scaleY = 0.25;

            //フェードスピード
            this._FadeSpeed = 0.02;
    
        },
        onenterframe : function()
        {
            if(this.opacity >= 0.02)
            {
                this.opacity -= this._FadeSpeed;
            }
            else if(this.opacity <= 0.02)
            {
                this.opacity = 0.01;
                this.scene.removeChild(this);
            }
            this.y -= 0.5;
        }
    });