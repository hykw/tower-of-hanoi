enchant();

var Player = enchant.Class.create(enchant.Sprite,
{
    initialize : function(x,y,scale, game)
    {
        //enchant.Sprite.call(this,580 / 3,580 / 3);    
        enchant.Sprite.call(this, 160, 140);


        this.scaleX = scale;
        this.scaleY = scale;
        
        this.x = x;
        this.y = y;

        this.image = game.assets[imageFrog];
        this.frame = 0;

        //選択されているか
        this._chooseFlag = false;
        
        this._flagstora = false;
        //0はなにもなし　1,2,3のidを入れる
        this._id = 0;
        //0,1,2で管理
        this._dodaiId = 0;

        this._flag = false;
        this._game = game;
    },

    //Updateの様なもの
    onenterframe : function()
    {
        var time = parseInt(this._game.frame / this._game.fps);
        if(this._chooseFlag)
        {
            if(this._flag)
            {
                this.frame = 1;

                if(time % 2 != 0)
                {
                    this._flag = false;
                }
            }
            else
            {
                this.frame = 2;

                if(time % 2 == 0)
                {
                    this._flag = true;
                }
            }
        }
        else
        {
            this.frame = 0;
        }
    }
});