enchant();

var TitleButton = enchant.Class.create(enchant.Sprite,
{
    initialize : function(x, y, width, height, game, Image)
    {
        enchant.Sprite.call(this, width, height);

        this.x = x;
        this.y = y;

        this.scaleX = 1.2;
        this.scaleY = 1.2;

        this.image = game.assets[Image];
        this.frame = 0;

        this._sceneFlag = false;
    },

    ontouchstart : function()
    {
        //game.replaceScene(scene);
        this._sceneFlag = true;
    }
});