var items = [];

var ScoreItem = enchant.Class.create(enchant.Sprite,
{
        initialize:function(x,y,game)
    {
        enchant.Sprite.call(this,116,325 / 2);

        this.x = x;
        this.y = y;

        this.image = game.assets[imageApple];
        this.frame = 7;

        this._Speed = 0.5;

        this._itemFlag = false;
        this._dodaiId = 0;
        this._hitFlag = true;
        this._score = 0;
    },
    onenterframe : function()
    {
        if (this.y < -200 || 360 < this.y)
        {
            this.scene.removeChild(this);
            items.splice(this, 1);
        }
        if(this.y > 340)
        {
            this.opacity -= 0.05;
            this._hitFlag = false;
        }
        if(this.opacity <= 0.02)
        this.opacity = 0.01;
        this.y += this._Speed;
        //var randY = Math.random();
        //this.y += randY;
    }
});